function [ Es ] = gen_events_all (sd, ed, intdt, intwd0, ts_uc, Uccs, paraevents )
%Generation of events for all demand categories of demand type (eletric %resp. dhw). 
%result: event list Es


%Initialisation
Es = [];    %Event serie
clear dn   %day number
clear gp
clear ndc
clear a
clear k


gp = get_general(intdt);
ndc = gp.ndc;


%default event at start time with power change of zero for each category
for k = 1 : ndc
    Es(end + 1,:) = [NaN, NaN, NaN, intdt, k, 0, 0, 0, 0, 0, 0];
end %for


%loop, each loop run adds all the events (all categories) of one day to the Output file 'events_el'
%resp. 'events_hw' 
for dn = sd:ed  %one iteration per day 
    
    %loop initialisation
    clear enwd
    clear intwd
    clear j
    clear i
    clear temp
    clear temp2
    
    %determination of weekday
    intwd = get_weekday (dn,intwd0);
    
    %Expand Es, 
    temp = gen_events_all_day (dn, intwd, intdt, ndc, ts_uc, Uccs, paraevents);
    Es = [Es; temp];  %each loop run adds all the events of one day to the list E_El'

end %end for, one iteration per day


%default event at end time with power change of zero
for k = 1 : ndc
    Es(end + 1,:) = [NaN, NaN, NaN, intdt, k, 24, ((ed - sd) .* 24) + 24, 0, 0, 24, ((ed - sd) .* 24) + 24];  
end %for

%---%Replace NaN values in Es
a = size(Es);

%Replace column 7 (NaN) with calculated time t1period
Es(:,7) = Es(:,6) + ((Es(:,2) - sd) .* 24);    %t1period = t1day + ((dn - sd)*24)

%Replace column 11 (NaN) with calculated time t2period
Es(:,11) = Es(:,10) + ((Es(:,2) - sd) .* 24);    %t2period = t2day + ((dn - sd)*24)

%Replace first column (NaN) with event counter (eno)
eno = (1:1:a(1))';    %column vector with column length of Es, starting from 0, incrementing by 1
Es(:,1) = eno;
clear eno

%cleanup Replace NaN values in Es
clear a b
clear intdt
clear dn


%---Sort event list 
Es = sortrows (Es, 7); %sort on ascending time  

end %function gen_events_all
