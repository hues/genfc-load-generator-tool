function [intwd] = get_weekday (dn,intwd0)
%determination of weekday

intwd_temp = rem(dn + intwd0 - 1, 7);

if intwd_temp == 0
    intwd = 7;
else
    intwd = intwd_temp;
end %if 

end %function