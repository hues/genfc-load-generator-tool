function [] = read_occupancy_excel(intdt)
%Read in of parameters for events.

%---set type dependent variable names
switch intdt
    case 1
        strvariablename = 'el_inputfilenames_occupancy';
        strMd = 'el_Md';
    case 2
        strvariablename = 'hw_inputfilenames_occupancy';
        strMd = 'hw_Md';
end %switch

%---initialise
eval(['global ', strvariablename])  %e.g.: global el_inputfilenames_occupancy
eval(['global ', strMd])  %e.g.: global el_Md
clear dt_Md         %copy of el_Md resp. hw_Md
clear Occ_anr
clear Occ_aw
clear k
clear kk
clear n
clear nn
clear a
clear b
clear filename

gp = get_general(intdt);

%---read in
for k = 1 : gp.nk   %loop per dwelling

    %---read in of occupancy file for one dwelling
    eval(['filename = ', strvariablename, '{1,k};'])  %filename = el_inputfilenames_occupancy{1,k};

    Occ_anr = xlsread(filename,'readanr'); %,'','basic'
    Occ_aw = xlsread(filename,'readaw'); %,'','basic'

    
    %---read in of parameters for non-recurring absences
    dt_Md{1,k}.occupancy.Anrs = Occ_anr(:,1:7); %second index: column number in sheet anr of file occupancy_...xls
    %delete all rows with duration = 0;
    a = find(dt_Md{1,k}.occupancy.Anrs(:,3) == 0);
    dt_Md{1,k}.occupancy.Anrs(a,:) = [];
    clear a

     
    %---read in of parameters for recurring absences
    dt_Md{1,k}.occupancy.Aws = Occ_aw(:,1:6);    %second index: column number in sheet anr of file occupancy_...xls
    %delete all rows with duration = 0;
    a = find(dt_Md{1,k}.occupancy.Aws(:,3) == 0);
    dt_Md{1,k}.occupancy.Aws(a,:) = [];
    clear a
    
    
    %---check for probability equal zero
    %check for non-recurring absences, warning line output
    a = find(dt_Md{1,k}.occupancy.Anrs(:,7) == 0);
    if ~isempty(a)
        disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Warning: Please check probability for non-recurring absences. At least one probability is set to zero'])
    end %if
    clear a
    %check for recurring absences, warning line output
    a = find(dt_Md{1,k}.occupancy.Aws(:,6) == 0);
    if ~isempty(a)
        disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Warning: Please check probability for recurring absences. At least one probability is set to zero'])
    end %if
    clear a

    
    %---fill As, As is container for all absences
    
    %---fill As with non-recurring absences
    dt_Md{1,k}.occupancy.As = dt_Md{1,k}.occupancy.Anrs;
    
    %---fill As: append rows to As according to weekly recurring absences
    %convert recurring absences into non-recurring absences, Aws is than not used anymore
    a = size(dt_Md{1,k}.occupancy.Aws);
    for n = 1:a(1)
        intwd = dt_Md{1,k}.occupancy.Aws(n,1);
        days_wd = [];       %column vector for day numbers corresponding to a certain weekly absence
        for nn = gp.sd : gp.ed 
            day_wd = get_weekday(nn, gp.intwd0);
            if day_wd == intwd 
                days_wd = [days_wd;nn];
            end %if
        end %for

        %append to As
        clear temp
        clear b
        b = size(days_wd);
        for nnn = 1:b(1)
            temp(nnn,:) = [days_wd(nnn)', dt_Md{1,k}.occupancy.Aws(n,2), days_wd(nnn)', dt_Md{1,k}.occupancy.Aws(n,3:6)];
        end %for
        dt_Md{1,k}.occupancy.As = [dt_Md{1,k}.occupancy.As; temp];
    end %for 
    

    %---tune As
    %convert values given in unit "day" to unit "hour", counting from simulation start on, and adding column time of the day
    dt_Md{1,k}.occupancy.As(:,1) = (dt_Md{1,k}.occupancy.As(:,1) - gp.sd) .* 24 + dt_Md{1,k}.occupancy.As(:,2);
    dt_Md{1,k}.occupancy.As(:,3) = (dt_Md{1,k}.occupancy.As(:,3) - gp.sd) .* 24 + dt_Md{1,k}.occupancy.As(:,4);
    %convert time shift value given in unit "day" to unit "hour"
    dt_Md{1,k}.occupancy.As(:,5) = dt_Md{1,k}.occupancy.As(:,5) .* 24;
    %remove columns time of the day -> new array with start time of absence in column 1 and end time in column 2
    dt_Md{1,k}.occupancy.As(:,2) = []; 
    dt_Md{1,k}.occupancy.As(:,3) = []; 
    
    %check consistency of occupancy data (end time > start time)
    if ~isempty(find (dt_Md{1,k}.occupancy.As(:,1) > dt_Md{1,k}.occupancy.As(:,2)))
        msgbox (['Please check occupancy data for dwelling ' num2str(k), '. End time must be later than start time.'])
    end %if
       
    
    %---assing el_Md resp. hw_Md
    eval([strMd,'{1,k}.occupancy = dt_Md{1,k}.occupancy;'])     %e.g. el_Md{1,k}.occupancy = dt_Md{1,k}.occupancy

    
    %---display confirmation message
    disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: File "', filename,'" successfully read in.'])
    clear filename

end %for     


%---clean up
clear dt_Md
clear Occ_anr
clear Occ_aw
clear k
clear kk
clear a
clear filename
clear p0


end %function
