function [tnoe] = calculate_tnoe(intdt)
%Calculate total number of events over all dwellings and demand categories for the simulation period
%takes in account matrix on top in excel ...pe.xls; e.g. holidays not
%considered
%only used to provide sufficient random numbers


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
    case 2
        strMd = 'hw_Md';
end %switch
    

%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
clear k kk
clear gp
clear tnoe

gp = get_general(intdt);
tnoe = 0;


%---add number of events for one week over all dwellings

%dwelling loop
for k = 1 : gp.nk
    %e.g.: tnoe = tnoe +  sum(sum(Md{1,k}.pe.Enwds));
    eval([' tnoe = tnoe +  sum(sum(', strMd, '{1,k}.pe.Enwds));'])
end %for

%---extrapolates weekly result to simulation periode
tnoe = round(tnoe .* ((gp.ed - gp.sd + 1) ./ 7));


end %function         
