function [agg] = agg_load_dwellings_dcs(intdt, dws, dcs)
%Aggregate events for dwellings and demand categories and calculate resulting overall Ls and Lc.
%If only one argument is delivered the aggregation is done for all
%dwellings and demand categories and results are written using write_total
%dws: vector with dwelling identifiers (number), optional, value 0
%for all dws
%dcs: vector with demand categories, optional, not yet implemented, value 0
%for all dcs


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
    case 2
        strMd = 'hw_Md';
end %switch


%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
eval(['global ', strMd, '_total'])  %e.g.: global el_Md_total, used only for writing and make available results to workspace
eval(['global ', strMd, '_agg'])  %e.g.: global el_Md_total, used only for writing and make available results to workspace
clear agg
agg.Es =[];
agg.Ls =[];
agg.Lc =[];
agg.p0 =[];
agg.p0_dcs =[];

gp = get_general(intdt);

p0 = 0;

%assign missing arguments
switch nargin
    case 1
        %assign vector dws with all dwelling numbers
        dws = 0;
         for k = 1 : gp.nk  %loop over dwellings
            dws = [dws, k];
        end %for

        %assign vector dcs with all dwelling numbers
        dcs = 0;
        for kk = 1 : gp.ndc  %loop over dwellings
            dcs = [dcs, kk];
        end %for
    case 3
        %assign vector dws with all dwelling numbers in case of argument value 0 for dws
        if dws==0
            for k = 1 : gp.nk  %loop over dwellings
                dws = [dws, k];
            end %for        
        end %if
        
        %assign vector dws with all dwelling numbers in case of argument value 0 for dws 
        if dcs==0;
            for kk = 1 : gp.ndc  %loop over dwellings
                dcs = [dcs, kk];
            end %for
        end %if
    otherwise
        msgbox('Please check arguments; agg_load_dwellings_dcs(intdt, [dws], [dcs])') 
end %switch


%---aggregate events
%loop over demand categories 
for kk = 1 : gp.ndc  
    if ~isempty(find(dcs==kk))
        
        %aggregation per demand category
        agg.Es_dcs{1,kk} = [];   %initialisation
        for k = 1 : gp.nk  %loop over dwellings
            
            if ~isempty(find(dws==k))
               eval(['agg.Es_dcs{1,kk} = [agg.Es_dcs{1,kk}; ', strMd, '{1,k}.Es_dcs{1,kk}];'])        %e.g. agg.Es_dcs{1,kk} = [agg.Es_dcs{1,kk};  el_Md{1,k}.Es_dcs{1,kk}]; 
            end %if
            
        end %for

        %aggregation of category totals
        agg.Es = [agg.Es; agg.Es_dcs{1,kk}];

    end %if
end %for
      

%---sort events by accending time
agg.Es = sortrows (agg.Es, 7);

for kk = 1 : gp.ndc  %loop demand category
   if ~isempty(find(dcs==kk))
       agg.Es_dcs{1,kk} = sortrows (agg.Es_dcs{1,kk}, 7);
   end  %if
end %for


%---sum standby loads
agg.p0 = 0;
for kk = 1 : gp.ndc  %loop demand category    
   if ~isempty(find(dcs==kk))
       
        %sum standby loads for one category
        agg.p0_dcs(1,kk) = 0;
        for k = 1 : gp.nk  %loop for one demand category
            
            if ~isempty(find(dws==k))
                eval(['agg.p0_dcs(1,kk) = agg.p0_dcs(1,kk) + ', strMd, '{1,k}.pe.p_standby(1,kk);'])        %agg.p0_dcs(1,kk) = agg.p0_dcs(1,kk) + el_Md{1,k}.pe.p_standby(1,kk);
            end %if
            
        end    %for

        %adding of category standby totals
        agg.p0 = agg.p0 +agg.p0_dcs(1,kk);    
        
   end %if
end %for


%---generate load series (event based time intervals)

%generate load serie over all dwellings and categories
p0 = agg.p0;
[agg.Ls, agg.Chs] = gen_loadserie_all( agg.Es, p0 );

%generate load serie per dwelling over all categories
for kk = 1 : gp.ndc  %loop demand category
   if ~isempty(find(dcs==kk))
        p0 = agg.p0_dcs(1,kk);
        [agg.Ls_dcs{1,kk}, agg.Chs_dcs{1,kk}] = gen_loadserie_all( agg.Es_dcs{1,kk}, p0 );
   end %if
end %for


%---generate load curves (fixed time intervals ts_lc)

%generate load serie over all dwellings and categories
p0 = agg.p0;
[agg.Lc] = gen_loadcurve_all (agg.Ls, gp.sd, gp.ed, intdt, gp.ts_lc, p0);

%generate load serie per dwelling over all categories
for kk = 1 : gp.ndc  %loop demand category
   if ~isempty(find(dcs==kk))    
       p0 = agg.p0_dcs(1,kk);
       [agg.Lc_dcs{1,kk}] = gen_loadcurve_all (agg.Ls_dcs{1,kk}, gp.sd, gp.ed, intdt, gp.ts_lc, p0);
   end  %if
end %for


switch nargin
    case 1
        
        %---assign global variable el_Md_total resp. md_Md_total
        eval([strMd, '_total = agg;'])      %e.g. hw_Md_total = agg;          
        
        %---write files
        eval(['write_total (''Es'', ', strMd, '_total.Es, 0, intdt);'])    %e.g. write_total ('Es', el_Md_total.Es, 0, intdt);
        eval(['write_total (''Ls'', ', strMd, '_total.Ls, 0, intdt);'])
        eval(['write_total (''Lc'', ', strMd, '_total.Lc, 0, intdt);'])

        for kk = 1 : gp.ndc  %loop demand category        
            eval(['write_total (''Es'', ', strMd, '_total.Es_dcs{1,kk}, kk, intdt);'])    %e.g. write_total ('Es', el_Md_total.Es_dcs{1,kk}, kk, intdt);
            eval(['write_total (''Ls'', ', strMd, '_total.Ls_dcs{1,kk}, kk, intdt);'])
            eval(['write_total (''Lc'', ', strMd, '_total.Lc_dcs{1,kk}, kk, intdt);'])
        end %for
        
        %---display confirmation message
        disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: Totals over all dwellings were successfully calculated and written to text files.'])

    case 3
        
        %---assign global variable el_Md_agg resp. md_Md_agg
        eval([strMd, '_agg = agg;'])      %e.g. hw_Md_agg = agg;          
        
        %---write files
        eval(['write_agg (''Es'', ', strMd, '_agg.Es, dws, dcs, intdt);'])    %e.g. write_agg ('Es', el_Md_agg.Es, dws, dcs, intdt);
        eval(['write_agg (''Ls'', ', strMd, '_agg.Ls, dws, dcs, intdt);'])
        eval(['write_agg (''Lc'', ', strMd, '_agg.Lc, dws, dcs, intdt);'])

        for kk = 1 : gp.ndc  %loop demand category
            if ~isempty(find(dcs==kk))
                eval(['write_agg (''Es'', ', strMd, '_agg.Es_dcs{1,kk}, dws, kk, intdt);'])    %e.g. write_agg ('Es', el_Md_agg.Es_dcs{1,kk}, kk, intdt);
                eval(['write_agg (''Ls'', ', strMd, '_agg.Ls_dcs{1,kk}, dws, kk, intdt);'])
                eval(['write_agg (''Lc'', ', strMd, '_agg.Lc_dcs{1,kk}, dws, kk, intdt);'])
            end  %if
        end %for
        
      %---display confirmation message
        disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: Totals over specified dwellings and categories were successfully calculated and written to text files.'])
        
end %switch

end %function
