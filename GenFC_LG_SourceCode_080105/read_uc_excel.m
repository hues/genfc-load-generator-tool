function [] = read_uc_excel(intdt)
%Read in of use curve files.

%---set type dependent variable names
switch intdt
    case 1
        strvariablename = 'el_inputfilenames_uc';
        strMd = 'el_Md';
    case 2
        strvariablename = 'hw_inputfilenames_uc';
        strMd = 'hw_Md';
end %switch

%---initialise
eval(['global ', strvariablename])  %e.g.: global el_inputfilenames_pe
eval(['global ', strMd])  %e.g.: global el_Md
clear dt_Md         %copy of el_Md resp. hw_Md
clear Uc
clear k
clear kk
clear filename

gp = get_general(intdt);

%---read in
for k = 1 : gp.nk   %loop per dwelling
    for kk = 1 : 7    %loop per weekday, mo: k = 1; ...; su: k = 7

        %---read in of a weekday specific use curve file for one dwelling
        eval(['filename = ', strvariablename, '{kk,k};'])          %filename = el_inputfilenames_uc{kk,k};
        
        Uc = xlsread(filename,'readuc');  %,'','basic'

        %assigning dt_Md
        dt_Md{1,k}.Ucs{kk} = Uc;        

        %---display confirmation message
        disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: File "', filename,'" successfully read in as ', intwd2strwd(kk),'-usecurve for dwelling ', int2str(k), '.'])
        clear filename
    
    end %for

    %assing el_Md resp. hw_Md
    eval([strMd,'{1,k}.Ucs = dt_Md{1,k}.Ucs;'])     %e.g. el_Md{1,k}.Ucs = dt_Md{1,k}.Ucs

end %for        


%---clean up
clear dt_Md
clear Uc
clear k
clear kk
clear filename


end %function