function [] = calculate_load (intcalc)
%Calculates Md, including Md.Es_dcs, Md.Ls_dcs, Md.Lc_dcs.
%intcalc: value 1 -> only electric load
%intcalc: value 2 -> only hot water load
%intcalc: value 3 -> both electric and hot water load

%---initialise
clear global *      %clears all global variables in the workspace and functions
clear k
clear k_start
clear k_end

%---contol calculation (el and/or hw)
switch intcalc
    case 1
        k_start = 1;
        k_end = 1;
    case 2
        k_start = 2;
        k_end = 2;
    case 3
        k_start = 1;
        k_end = 2;
end %switch


t1_sim = clock;
for k = k_start : k_end       %first loop for electric load, second loop for hot water loop
    %initialise loop
    intdt = k;     %electric load resp. hot water load
    
    
    %---read in of parameters
    read_general_excel(intdt)
    read_inputfilenames_pe_excel(intdt)
    read_pe_excel(intdt)
    read_inputfilenames_uc_excel(intdt)
    read_uc_excel(intdt)
    read_inputfilenames_occupancy_excel(intdt)
    read_occupancy_excel(intdt)
    
    
  
    
    %---generation of ramdom number lists
    clear n      %temporally used for number of random numbers 
    n = round(calculate_tnoe(intdt) .* 5.1);     %factor 5.1 is to assure that there are in any case sufficient random numbers generated, rn per event: daytime, mod. en, mod. power (2 rn), mod. duration (2 rn)
    
    gen_Rne(n); %generates list with random numbers, uniformlly distributed in intervall 0 to 1;
    gen_Rnn(n); %generates list with random numbers, normally distributed;

    clear n;
    
    
    %---simulation, writeout
    gen_Uccs(intdt)
    gen_Es_dwellings_all(intdt)
    gen_occupancy(intdt)
    gen_occupancy_filter_events(intdt)
    gen_L_dwellings_all(intdt)
    assign_events_dwellings_dcs(intdt)  
    gen_loadseries_dwellings_dcs(intdt)
    gen_loadcurves_dwellings_dcs(intdt)
    write_results_excel(intdt)
    agg_load_dwellings_dcs(intdt);

    
    %---clean up loop
    clear intdt
        
end %for

%---clean up
clear k


%---display simulation time
t2_sim = clock;
t_sim = etime(t2_sim, t1_sim);
disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Simulation completed; simulation runtime [s]: ', num2str(t_sim)])

%cleanup
clear t1_sim
clear t2_sim
clear t_sim
clear dt_Md


end %function