function [] = assign_events_dwellings_dcs(intdt)
%Expand el_Md resp. hw_Md with category specific events.


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
    case 2
        strMd = 'hw_Md';
end %switch

%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
clear a
clear k


%---assign events
eval(['a = size (', strMd, ');'])    %a = size (el_Md);

for k = 1 : a(2)     %loop dwelling
    %initialise loop
    eval(['clear ', strMd, '{1,k}.Es_dcs'])
    eval(['clear ', strMd, '{1,k}.Chs_dcs'])
    
    %assign events
    eval([strMd, '{1,k}.Es_dcs = get_events_dwelling_dcs (intdt, ', strMd, '{1,k}.Es);'])    %el_Md{1,k}.Es_dcs = get_events_dwelling_dcs (intdt, el_Md{1,k}.Es);
    eval([strMd, '{1,k}.Chs_dcs = get_events_dwelling_dcs (intdt, ', strMd, '{1,k}.Chs);'])
end %for


%---clean up
clear a
clear b
clear k


end %function