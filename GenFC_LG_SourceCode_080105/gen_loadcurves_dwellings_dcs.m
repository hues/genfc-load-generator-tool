function [] = gen_loadcurves_dwellings_dcs(intdt)
%Generate Lc for specified load categories and specified dwellings -> el_Md.Lc_dcs resp. hw_Md.Lc_dcs.


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
    case 2
        strMd = 'hw_Md';
end %switch

%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
clear a
clear c
clear k
clear kk
clear Ls
clear p0


gp = get_general(intdt);

%---generate Lc
eval(['a = size(', strMd, ');'])    %a = size(el_Md);

for k = 1 : a(2)  %loop dwelling
    eval(['c = size(', strMd, '{1,k}.Ls_dcs);'])     %c = size(el_Md{1,k}.Ls_dcs);
    for kk = 1 : c(2)   %loop demand category
        eval(['Ls = ', strMd, '{1,k}.Ls_dcs{1,kk};'])       %Ls = el_Md{1,k}.Ls_dcs{1,kk};
        eval(['p0 = ', strMd, '{1,k}.pe.p_standby(kk);'])        %p0 = el_Md{1,k}.pe.p_standby(kk);
        eval([strMd, '{1,k}.Lc_dcs{1,kk} = gen_loadcurve_all (Ls, gp.sd, gp.ed, intdt, gp.ts_lc, p0);'])    %        el_Md{1,k}.Lc_dcs{1,kk} = gen_loadcurve_all (Ls, gp.sd, gp.ed, intdt, gp.ts_lc, p0);
    
        
        %---write files
        eval(['write_dcs (''Es'',', strMd, '{1, k}.Es_dcs{1,kk}, k, kk, intdt);'])    %e.g. write_all ('Es', el_Md{1, k}.Es_dcs{1,kk}, k, kk, intdt);
        eval(['write_dcs (''Ls'',', strMd, '{1, k}.Ls_dcs{1,kk}, k, kk, intdt);'])    %e.g. write_all ('Ls', el_Md{1, k}.Ls_dcs{1,kk}, k, kk, intdt);
        eval(['write_dcs (''Lc'',', strMd, '{1, k}.Lc_dcs{1,kk}, k, kk, intdt);'])    %e.g. write_all ('Lc', el_Md{1, k}.Lc_dcs{1,kk}, k, kk, intdt);
    
    end %for kk
end %for k


%---display confirmation message
disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: Loads per category were successfully generated and written to files.'])


%---clean up
clear a
clear c
clear k
clear kk
clear Ls


end %function