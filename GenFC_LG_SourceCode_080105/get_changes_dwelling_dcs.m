function [Chs_dcs] = get_changes_dwelling_dcs (intdt, Chs, dcs)
%Retrieve change series for selected demand categories.
%dcs   demand categories(s), vector, e.g. [3, 5, 6, 7]; optional

clear k
clear m
clear n
clear nk
clear Chs_dc
clear Chs_dcs
clear gp

switch nargin
    case 2
        gp = get_general(intdt);
        
        nk = gp.ndc;
               
        for k = 1 : nk
            Chs_dc = get_changes_dwelling_dc (k);
            Chs_dcs(k) = {Chs_dc};
        end %for        
    case 3
        m = size (dcs);
        nk = m(2);

        for k = 1 : nk
            Chs_dc = get_changes_dwelling_dc (dcs(k));
            Chs_dcs(k) = {Chs_dc};
        end %for
        
    otherwise
end %switch


    
function [Chs_dc] = get_changes_dwelling_dc (dc)

clear Chs_dc
clear a

a = find ((Chs(:,5)) == dc);
Chs_dc = Chs(a(1),:); %rows that fullfill criteria dc   
    
end %function get_events_dwelling_dc


end %function get_events_dwelling_dcs
