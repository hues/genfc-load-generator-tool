function [ Lc ] = gen_loadcurve_all (Ls, sd, ed, intdt, ts_lc, p0)
%Generation of loadcurve (fixed time intervals) based on loadseries (event based time intervals).


%Initialisation
clear m
clear temp
clear temp2
clear p
clear Lc
clear k
clear j
clear t
clear lno
clear nts %number of timesteps
clear l   %load (row of Ls)
clear a
clear b
clear i
clear p0_int  %power at start of interval
clear gp
clear ndc


gp = get_general(intdt);


%Body
nts = ((ed - sd + 1) .* 24) ./ ts_lc;    %period starts/ends at midnight
m = size (Ls);
temp = ones (nts, m(2)) .* (-500);  %prealocation of array, value "-500" as placeholder for no overwritten values


%generate vector t with start time of interval, plus end time
t(1) = 0; %p(1) refers to record for t = 0
for j = 1 : nts
    t(j+1) = t(j) + ts_lc;                   %t(2) = 0 + ts_lc   
end %for

%generate matrix temp with rows of Ls with first power value within each time intervall, for
%last intervall: row of Ls with last power value

clear temp2 %rows of Ls within time interval interval 
clear p_avg

%---set first row of Lc, and use it to initialise temp2
temp2 = [1, gp.sd, gp.intwd0, NaN, NaN, 0, 0, NaN, p0];    
p0_int = p0;

for k = 1 : (nts + 1)
  
    % get first row of Ls within time interval tc_lc, in case of no
    % records: get last row of previous iteration
    %Initialise
    clear a
    clear b
    
    %---find all records Ls in intervall ts_lc
    if isempty (Ls)  %prevent error in case of empty Ls
        a = [];
    else
        [a, b] = find(t(k) <= Ls(:,7) & Ls(:,7) <= (t(k) + ts_lc));  
    end %if
   
    %row "temp" is only set to new value (first row of Ls-rows within time
    %interval) in case of existing Ls-rows within time interval
    if isempty (a)  
        if k==1
            temp(1,:) = temp2;
        else
            temp(k,:) = temp2(end,:);  %row temp is set to the last row of the rows within ts_lc in the previous loop iteration, as the mean in the actual interval (which has no events) is equal to this last power value
        end
    else
        temp2 = Ls(a,:);
        p_avg = mean_of_interval ((temp2(:,7) - t(k)), temp2(:,9), p0_int, ts_lc);
        temp(k,:) = temp2(1,:);  
        temp(k,9) = p_avg;
        temp(k,2:5) = NaN;
        temp(k,8) = NaN;
        p0_int = temp2(end,9);   %initialisation for next loop, value of last row within acutal interval
    end % if
     
    temp(k,7) = t(k);  %time is set to the time frame defined by ts_lc (original tperiod of Ls overritten)
    temp(k,6) = rem(t(k), 24);   %time is set to the time frame defined by ts_lc (original tday of Ls overritten)

end %for

Lc = temp;

%----------
%Replace columns of Lc
clear m
clear lno
m = size (Lc);

%Replace first column with counter for rows of Lc (lno)
lno = (1:1:m(1))';    %column vector with column length of Ls, starting from 1, incrementing by 1
Lc (:, 1) = lno;

%Replace column 8 (ch) with values "NaN", Not a number"
Lc (:, 8) = NaN;

end %function gen_loadcurve_all



function [avg] = mean_of_interval ( t, p, p0_int, ts_lc)
%t: vector with timepoints, t=0 at begin of interval
%p: vector with power values
%p0_int: used for first product within interval
%ts_lc: used for last product within interval

clear avg
clear m
m = size ( t );
clear k
clear product
clear sum_product

sum_product = 0;

%first product within interval
product = t(1) .* p0_int;
sum_product = sum_product + product;

%intermediate products within interval
for k = 2 : m(1)
    product = (t(k) - t(k-1)) .* p(k-1);
    sum_product = sum_product + product;
end %for

%last product within interval
product = (ts_lc - t(m(1))) .* p(m(1));
sum_product = sum_product + product;

%calculate average
avg = sum_product ./ ts_lc;
    
end %function


