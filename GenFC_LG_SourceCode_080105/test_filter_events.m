function [] = test_filter_events(intdt)



%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
        strMd_total = 'el_Md_total';
    case 2
        strMd = 'hw_Md';
        strMd_total = 'hw_Md_total';
end %switch


%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
k = 1;
n_e = 1;
n_f = 5;

clear rows_delete 
clear events_start
clear a
clear b
clear kk

a = size(hw_Md{1,k}.occupancy.As_eff);
for kk = 1 : a(1)       %loop per row of As_eff
    n_f = kk;
    
    events_start = hw_Md{1,k}.Es(:,7);

    filter_start = hw_Md{1,k}.occupancy.As_eff(n_f,1);
    filter_end = hw_Md{1,k}.occupancy.As_eff(n_f,2);

    rows_delete = find(filter_start < events_start & events_start < filter_end);

    %---message for test purpuses
    %     b = size(rows_delete);
    %     msgbox([num2str(b(1)),' events will be deleted'])

    hw_Md{1,k}.Es(rows_delete,:) = [];
end %for    
    
end     %function