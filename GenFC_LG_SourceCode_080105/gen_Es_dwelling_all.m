function [Es] = gen_Es_dwelling_all (sd, ed, intdt, intwd0, ts_uc, ts_lc, Uccs, paraevents) 
%Generate load for one dwelling, all demand categories.

clear p0;


Es = gen_events_all (sd, ed, intdt, intwd0, ts_uc, Uccs, paraevents);

% 
% 
% 
% %----------------------
% S(1).Es =  Es;   %S contains only S(1), overwritten for each dwelling run

end    %function
