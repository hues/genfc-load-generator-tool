function varargout = loadgengui_oneWindow(varargin)
% LOADGENGUI_ONEWINDOW M-file for loadgengui_oneWindow.fig
%      LOADGENGUI_ONEWINDOW, by itself, creates a new LOADGENGUI_ONEWINDOW or raises the existing
%      singleton*.
%
%      H = LOADGENGUI_ONEWINDOW returns the handle to a new LOADGENGUI_ONEWINDOW or the handle to
%      the existing singleton*.
%
%      LOADGENGUI_ONEWINDOW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOADGENGUI_ONEWINDOW.M with the given input arguments.
%
%      LOADGENGUI_ONEWINDOW('Property','Value',...) creates a new LOADGENGUI_ONEWINDOW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before loadgengui_oneWindow_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to loadgengui_oneWindow_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help loadgengui_oneWindow

% Last Modified by GUIDE v2.5 24-Oct-2007 16:29:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @loadgengui_oneWindow_OpeningFcn, ...
                   'gui_OutputFcn',  @loadgengui_oneWindow_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before loadgengui_oneWindow is made visible.
function loadgengui_oneWindow_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to loadgengui_oneWindow (see VARARGIN)

% Choose default command line output for loadgengui_oneWindow
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Custom initialisation function
initialize_gui(hObject, handles, false);


% UIWAIT makes loadgengui_oneWindow wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = loadgengui_oneWindow_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function initialize_gui(fig_handle, handles, isreset)
set(handles.uipanel_loadselection, 'SelectedObject', handles.rb_both);
handles.loadtype = 3;

% Update handles structure
guidata(handles.figure1, handles);




% --------------------------------------------------------------------
function uipanel_loadselection_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_LoadSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'Tag')   % Get Tag of selected object
    %hObject refers to the selected radio button 
    case 'rb_electric'
        handles.loadtype = 1;
    case 'rb_hotwater'
        handles.loadtype = 2;
    case 'rb_both'
        handles.loadtype = 3;
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_startsimulation.
function pb_startsimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pb_startsimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch handles.loadtype
    case 1
        main_el();
    case 2
        main_hw();
    case 3
        main_both();
end %switch
        
msgbox('Simulation completed, please check messages in DOS window', 'GenFC load generator.');



% --- Executes on button press in rb_electric.
function rb_electric_Callback(hObject, eventdata, handles)
% hObject    handle to rb_electric (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rb_electric




function edit_plotcommand_Callback(hObject, eventdata, handles)
% hObject    handle to edit_plotcommand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_plotcommand as text
%        str2double(get(hObject,'String')) returns contents of edit_plotcommand as a double
eval(get(hObject, 'String'))



% --- Executes during object creation, after setting all properties.
function edit_plotcommand_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_plotcommand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_savelastsimulation.
function pb_savelastsimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pb_savelastsimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on button press in pb_loadlastsimulation.
function pb_loadlastsimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pb_loadlastsimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load LG_LastSimulation.mat;
load_lastsimulation;
msgbox('Last simulation loaded, ready for plotting.');


% --- Executes on button press in pb_loadsimulation.
function pb_loadsimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pb_loadsimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uigetfile



% --- Executes on button press in pb_loadsimulation2.
function pb_loadsimulation2_Callback(hObject, eventdata, handles)
% hObject    handle to pb_loadsimulation2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uigetfile('*.mat','Select simulation result file', 'LG_LastSimulation.mat')



