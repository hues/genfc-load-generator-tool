function [strdt] = intdt2strdt(intdt)
%Converts demand-type-no. to demand-type-string.

clear dtdt;
clear strdt;

dtdt = struct('strdt', {'el', 'hw'});

strdt = dtdt(intdt).strdt;

end

