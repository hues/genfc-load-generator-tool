function [] = gen_loadseries_dwellings_dcs(intdt)
%Generate Ls for specified load categories and specified dwellings -> el_Md.Ls_dcs resp. hw_Md.Ls_dcs.


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
    case 2
        strMd = 'hw_Md';
end %switch

%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
clear a
clear c
clear k
clear kk
clear Es
clear p0   %standby power of respective demand category

gp = get_general(intdt);

%---generate Ls
eval(['a = size (', strMd, ');'])    %a = size (el_Md);

for k = 1 : a(2)  %loop dwelling
    eval(['c = size (', strMd, '{1,k}.Es_dcs);'])         %c = size (el_Md{1,k}.Es_dcs);
    for kk = 1 : c(2)  %loop demand category
        eval(['Es = ', strMd, '{1,k}.Es_dcs{1,kk};'])        %Es = el_Md{1,k}.Es_dcs{1,kk};
        eval(['p0 = ', strMd, '{1,k}.pe.p_standby(kk);'])    %p0 = el_Md{1,k}.pe.p_standby(kk);                
        eval([strMd, '{1,k}.Ls_dcs{1,kk} = gen_loadserie_all (Es, p0);'])       %el_Md{1,k}.Ls_dcs{1,kk} = gen_loadserie_all (Es, p0);
    end %for kk
end %for k        


%---clean up
clear a
clear c
clear k
clear kk
clear Es


end %function