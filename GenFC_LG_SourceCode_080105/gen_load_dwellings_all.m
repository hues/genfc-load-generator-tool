function [] = gen_load_dwellings_all(intdt)
%Generate load for all dwellings and demand categories.


%---set type dependent variable names
switch intdt
    case 1
        strvariablename = 'el_inputfilenames_uc';
        strMd = 'el_Md';
    case 2
        strvariablename = 'hw_inputfilenames_uc';
        strMd = 'hw_Md';
end %switch


%---initialise
eval(['global ', strvariablename])  %e.g.: global el_inputfilenames_pe
eval(['global ', strMd])  %e.g.: global el_Md
clear dt_Md         %copy of el_Md resp. hw_Md
clear k
clear B
clear gp
clear Uccs
clear paraevents    %variable name paraevents used as pe is reserved Matlab name

gp = get_general(intdt);


%---generate load
for k = 1 : gp.nk     %loop for dwelling 
    clear temp
    clear Uccs
    clear paraevents

    eval(['Uccs = ', strMd, '{1, k}.Uccs;'])     %e.g: Uccs = el_Md{1, k}.Uccs;
    eval(['paraevents = ', strMd, '{1, k}.pe;'])         %e.g: paraevents = el_Md{1, k}.pe;
    temp = gen_load_dwelling_all (gp.sd, gp.ed, intdt, gp.intwd0, gp.ts_uc, gp.ts_lc, Uccs, paraevents);
    dt_Md{1, k}.Es = temp.Es;
    dt_Md{1, k}.Chs = temp.Chs;
    dt_Md{1, k}.Ls = temp.Ls;
    dt_Md{1, k}.Lc = temp.Lc;

    
    %---assing el_Md resp. hw_Md
    eval([strMd,'{1,k}.Es = dt_Md{1,k}.Es;'])     %e.g. el_Md{1,k}.Es = dt_Md{1,k}.Es;
    eval([strMd,'{1,k}.Chs = dt_Md{1,k}.Chs;'])
    eval([strMd,'{1,k}.Ls = dt_Md{1,k}.Ls;'])
    eval([strMd,'{1,k}.Lc = dt_Md{1,k}.Lc;'])

    
    %---write files
    eval(['write_all (''Es'',', strMd, '{1, k}.Es, k, intdt);'])    %e.g. write_all ('Es', el_Md{1, k}.Es, k, intdt);
    eval(['write_all (''Ls'',', strMd, '{1, k}.Ls, k, intdt);'])
    eval(['write_all (''Lc'',', strMd, '{1, k}.Lc, k, intdt);'])

    
    %---display confirmation message
    disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: Dwelling ', num2str(k),', load was successfully generated and written to files.'])
end %end for




%---clean up
clear dt_Md
clear k
clear temp
clear Uccs
clear paraevents


end %function

