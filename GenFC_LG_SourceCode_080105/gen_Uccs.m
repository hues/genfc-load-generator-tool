function [] = gen_Uccs(intdt)
%Calculation of cumulated time use curve Ucc.

%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
    case 2
        strMd = 'hw_Md';
end %switch


%---initialise
eval(['global ', strMd])  %e.g.: global el_Md


%---calculation
gp = get_general(intdt);
for k = 1 : gp.nk   %loop per dwelling

    eval(['clear ', strMd, '{1,k}.Uccs;'])       %initialise loop
    
    for kk = 1 : 7    %loop per weekday, mo: k = 1; ...; su: k = 7
        eval([strMd, '{1,k}.Uccs{kk} = gen_Ucc(', strMd, '{1,k}.Ucs{kk});'])
%         switch intdt
%             case 1
%                 el_Md{1,k}.Uccs{kk} = gen_Ucc(el_Md{1,k}.Ucs{kk});
%             case 2
%                 hw_Md{1,k}.Uccs{kk} = gen_Ucc(hw_Md{1,k}.Ucs{kk});
%         end %switch
    end %for
end %for     


%---clean up
eval(['clear ', strMd])
clear kk


end %function