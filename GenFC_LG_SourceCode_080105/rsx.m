function [] = rsx(h, startday, endday, starttime, endtime )
%Rescales x-axis of specified figure, y-axis is unaffected.

%set focus to specified figure
figure(h)

%store settings of x-axis
v1_axis = axis; %store current min, max values; v(3): ymin; v(4): ymax 

%---rescale x-axis
switch nargin
    case 1
        axis tight
        v2_axis = axis;
        startday = v2_axis(1);
        endday = v2_axis(2);
        axis([startday, endday, v1_axis(3), v1_axis(4)])    
    case 3
        %check arguments
        if (endday - startday) < 0
            error ('please check arguments -> arguments: fig-id, [startday], [endday], [starttime], [endtime]')
        end %if
        
        axis([startday, endday, v1_axis(3), v1_axis(4)])
    case 4
        error ('invalid number of arguments -> arguments: fig-id, [startday], [endday], [starttime], [endtime]')
    case 5
        %recalculte arguments startday and endday considering starttime and
        %endtime
        startday = startday + starttime./24;
        endday = endday + endtime./24;
        
        %check arguments
        if (endday - startday) <= 0
            error ('please check arguments -> arguments: fig-id, [startday], [endday], [starttime], [endtime]')
        end %if
        
        %ditto as case 3
        axis([startday, endday, v1_axis(3), v1_axis(4)])
    otherwise
        error ('function rsx requires the following arguments: fig-id, [startday], [endday]')
end %switch

        
