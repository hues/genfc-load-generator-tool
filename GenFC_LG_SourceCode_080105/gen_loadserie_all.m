function [ Ls, Chs ] = gen_loadserie_all( Es, p0 )
%Generation of load serie and change serie based on event serie.


%case of empty Es -> assign empty array, return
if isempty (Es)
    Chs = ones(1,9); Chs(1,:) = [];   %assign empty 1*9 array
    Ls = ones(1,9); Ls(1,:) = [];     %assign empty 1*9 array
    return
end %if

Chs = gen_changes (Es );
Ls = integrate_changes (Chs, p0 );

end %function


function [Chs] = gen_changes (Es )

%Initialisation
clear m
clear temp
clear temp1
clear temp2
clear Chs

%Body
m = size (Es);
temp1(1:m(1),1) = 1;
temp2(1:m(1),1) = 2;
Chs_start = [Es(:,1:7), temp1, Es(:,9)];   %all on events
Chs_stop = [Es(:,1:5), Es(:,10:11), temp2, (0 - Es(:,9))];    %all off events, convert load values (plus -> minus)
Chs = [Chs_start; Chs_stop];  

%Sort change list 
Chs = sortrows (Chs, 7); %sort on ascending time 

%clean up
clear m
clear temp1

end %function gen_changes

function [ Ls ] = integrate_changes ( Chs, p0 )

%Initialisation
clear m
clear temp1
clear p
clear Ls
clear j
clear ch
clear lno
clear gp

m = size (Chs); %gp = get_general();
%p0 = gp.p0;
p = zeros(1,m(1));

%Body
%generate vector with aggregeated power value for each time of change

for j = 1 : (m(1))
  
    if j == 1
        p(j) = p0 + Chs(j,9);
    else
        p(j) = p(j-1) + Chs(j,9);
    end % if
    
end %for

Ls = [Chs(:,2:8), (p')];

%Add first column with change counter (chno)
lno = (1:1:m(1))';    %column vector with column length of Ls (equal to column length of Chs), starting from 1, incrementing by 1
Ls = [lno, Ls];

%Insert at beginning of Ls a row corresponding to t = t_start first row to Ls
%temp = Ls(1,4);  %demand type of first row, befor inserting row for t = 0
%Ls = [0, NaN, NaN, temp, NaN, 0, 0, NaN, p0; Ls]; 

%clean up
clear m
clear temp1
clear p
clear j
clear ch
clear lno

end %function integrate_changes





