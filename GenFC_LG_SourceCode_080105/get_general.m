function [gp] = get_general(intdt)
%Access global parameter 'general' which contains all general parameters. 
%Global variables 'el_general' resp. 'hw_general' are never accessed directly.
% gp.sd        start day
% gp.ed        end day
% gp.intwd0    weekday of start day (mo: 1; tu: 2; ...)
% gp.ts_uc     timestep of use curves
% gp.ts_lc     timestep of load curves e.g. 5 minutes: '5 ./ 60'
% gp.ndc       number of demand categries
% gp.nk        number of runs (= number of dwellings)


%---set type dependent variable names
clear strgeneral
switch intdt
    case 1
        strgeneral = 'el_general';     
    case 2
        strgeneral = 'hw_general';     
end %switch
    

%---initialise
eval(['global ', strgeneral])  %e.g.: global el_general
clear gp

%---assign values
eval(['gp.sd = ', strgeneral, '.sd;'])        %gp.sd = el_general.sd;
eval(['gp.ed = ', strgeneral, '.ed;'])        % gp.ed = general.ed;
eval(['gp.intwd0 = ', strgeneral, '.intwd0;'])        % gp.intwd0 = general.intwd0;
eval(['gp.ts_uc = ', strgeneral, '.ts_uc;'])        % gp.ts_uc = general.ts_uc;
eval(['gp.ts_lc = ', strgeneral, '.ts_lc;'])        % gp.ts_lc = general.ts_lc;
eval(['gp.ndc = ', strgeneral, '.ndc;'])        % gp.ndc = general.ndc;
eval(['gp.nk = ', strgeneral, '.nk;'])        % gp.nk = general.nk;


end %function get_general
