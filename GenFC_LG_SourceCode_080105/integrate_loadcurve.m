function [ demand ] = integrate_loadcurve (Lc)
%Integration of loadcurve -> energy demand.

clear product
clear demand
clear gp
clear ts_lc
clear t
clear k
clear m
clear n


t = Lc(:,7);
m = size(t);
p = Lc(:,9);
demand = 0;

%sum products 'timedifference * power'
for k = 2 : m(1)
    product = (t(k) - t(k-1)) .* p(k-1);
    demand = demand + product;
end %for

end %function integrate_loadcurve