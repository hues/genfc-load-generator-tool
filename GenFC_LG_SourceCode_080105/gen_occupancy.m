function [] = gen_occupancy(intdt)
%Effective absences are simulated based on input data As -> As_eff


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
    case 2
        strMd = 'hw_Md';
end %switch

%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
clear dt_Md         %copy of el_Md resp. hw_Md
clear k
clear kk
clear n
clear n_c   %counter for deletions
clear a
clear b
clear t_sh  %effective time shift column vector
clear t_d   %duration column vector[-], only used as placeholder for time difference
clear t_dch %effective relative duration change column vector[-], values 0 to 1


gp = get_general(intdt);


%---make dt_Md available to function
eval(['dt_Md = ', strMd, ';'])  %dt_Md = el_Md;


%---generation of effective absences
for k = 1 : gp.nk   %loop per dwelling
    
    
    %---prefill As_eff with As
    eval(['dt_Md{1,k}.occupancy.As_eff = ', strMd, '{1,k}.occupancy.As;'])  %dt_Md{1,k}.occupancy.As_eff = el_Md{1,k}.occupancy.As;
     
    
    %---determine size of As for use below in time shift and duration change
    a = size(dt_Md{1,k}.occupancy.As_eff);    
    
    %---time shift
    t_sh = dt_Md{1,k}.occupancy.As_eff(:,3) .* (rand(a(1),1) - (ones (a(1),1) .*.5)) .* 2;
    dt_Md{1,k}.occupancy.As_eff(:,3) = t_sh;
    dt_Md{1,k}.occupancy.As_eff(:,1) = dt_Md{1,k}.occupancy.As_eff(:,1) + t_sh;
    dt_Md{1,k}.occupancy.As_eff(:,2) = dt_Md{1,k}.occupancy.As_eff(:,2) + t_sh;
    
    %---duration change
    t_d = dt_Md{1,k}.occupancy.As_eff(:,2) - dt_Md{1,k}.occupancy.As_eff(:,1);
    t_dch = dt_Md{1,k}.occupancy.As_eff(:,4) .* (rand(a(1),1) - (ones (a(1),1) .*.5)) .* 2;
    dt_Md{1,k}.occupancy.As_eff(:,4) = t_dch;
    dt_Md{1,k}.occupancy.As_eff(:,2) = dt_Md{1,k}.occupancy.As_eff(:,2) + (t_dch .*  t_d);

    %---conditional absences (deleting of rows from As_eff with defined probability)
    n_c = 0;
    for n = 1 : a
        a_prob = dt_Md{1,k}.occupancy.As(n,5);  %note: importance to use here As (not As_eff) in order to delete the right row with the right probability
        %probabilistic deletion of row
        if rand(1) > a_prob
            dt_Md{1,k}.occupancy.As_eff(n - n_c,:) = [];  %delete actual row
            n_c = n_c + 1;
        end %if
    end %for
    
end %for     


%---assing el_Md resp. hw_Md
eval([strMd,'= dt_Md;'])     %e.g. el_Md = dt_Md;



%---display confirmation message
disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: Occupancy successfully simulated'])
   
    
%---clean up
clear dt_Md
clear k
clear kk
clear n
clear n_c
clear a
clear b
clear t_sh
clear t_d 
clear t_dch


end %function
