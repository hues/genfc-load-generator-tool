function [] = read_general_excel(intdt)
%Read in of general parameters.


%---set type dependent variable names
switch intdt
    case 1
        strfilename = 'el_general.xls';
        strvariablename = 'el_general';
    case 2
        strfilename = 'hw_general.xls';
        strvariablename = 'hw_general';
end %switch
    

%---initialise
eval(['global ', strvariablename])  %e.g.: global el_general


%---read in of inputfilenames for event parameters

%load filenames
eval(['[num, string] = xlsread(''', strfilename, ''',''readgp'');'])        %[num, string] = xlsread('el_general.xls','readgp');  %,'','basic'
eval([strvariablename, '.sd = num(1);'])       % el_general.sd = num(1);
eval([strvariablename, '.ed = num(2);'])        % el_general.ed = num(2);
eval([strvariablename, '.intwd0 = num(3);'])        % el_general.intwd0 = num(3);
eval([strvariablename, '.ts_uc = num(4);'])        % el_general.ts_uc = num(4);
eval([strvariablename, '.ts_lc = num(5);'])        % el_general.ts_lc = num(5);
eval([strvariablename, '.ndc = num(6);'])        % el_general.ndc = num(6);
eval([strvariablename, '.nk = num(7);'])        % el_general.nk = num(7);
clear string             

%display confirmation message

disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: File "', strfilename,'" successfully read in.'])


%---clean up
clear strfilename        
 

end %function