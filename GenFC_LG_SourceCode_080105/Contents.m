% _CODELG
%
% Files
%   assign_events_dwellings_dcs    - Expand el_Md resp. hw_Md with category specific events.
%   calculate_load                 - Calculates Md, including Md.Es_dcs, Md.Ls_dcs, Md.Lc_dcs.
%   calculate_tnoe                 - Calculate total number of events over all dwellings and demand categories for the simulation period
%   gen_events_all                 - Generation of events for all demand categories of demand type (eletric %resp. dhw). 
%   gen_events_all_day             - Generation of events for all demand categories of electric demand resp. dhw demand, yet with fixed.
%   gen_load_dwelling_all          - Generate load for one dwelling, all demand categories.
%   gen_load_dwellings_all          - Generate load for all dwellings and
%   demand categories.
%   gen_loadcurve_all              - Generation of loadcurve (fixed time intervals) based on loadseries (event based time intervals).
%   gen_loadcurves_dwellings_dcs   - Generate Lc for specified load categories and specified dwellings -> el_Md.Lc_dcs resp. hw_Md.Lc_dcs.
%   gen_loadserie_all              - Generation of load serie and change serie based on event serie.
%   gen_loadseries_dwellings_dcs   - Generate Ls for specified load categories and specified dwellings -> el_Md.Ls_dcs resp. hw_Md.Ls_dcs.
%   gen_Rne                        - Fill global pool 'Rne' with equally distributed random numbers.
%   gen_Rnn                        - Fill global pool 'Rnn' with normally distributed random numbers.
%   gen_Ucc                        - Generate cumulated use curve based on use curve.
%   gen_Uccs                       - Calculation of cumulated time use curve Ucc.
%   get_changes_dwelling_dcs       - Retrieve change series for selected demand categories.
%   get_events_dwelling_dcs        - Retrieve event series for selected demand categories.
%   get_general                    - Access global parameter 'general' which contains all general parameters. 
%   get_rne                        - Get equally distributed random number from Pool 'Rne'.
%   get_rnn                        - Get normally distributed random number from Pool 'Rnn'.
%   intdt2strdt                    - Converts demand-type-no. to demand-type-string.
%   integrate_loadcurve            - Integration of loadcurve -> energy demand.
%   intwd2strwd                    - Converts weekday-no. to weekday-string.
%   load_lastsimulation            - Makes simulation results in workspace available as global variables.
%   loadgengui_basic               - M-file for loadgengui_basic.fig
%   main_both                      - Controls calculation (el and/or hw) and makes simulation results accessible in workspace.
%   main_el                        - Controls calculation (el and/or hw) and makes simulation results accessible in workspace.
%   main_hw                        - Controls calculation (el and/or hw) and makes simulation results accessible in workspace. 
%   plot_all                       - Plot curves on level dwelling -> Es(Chs), Ls, Lc.
%   plot_dcs                       - Plot curves on demand level -> Es(Chs), Ls, Lc.
%   read_general_excel             - Read in of general parameters.
%   read_inputfilenames_pe_excel   - Read in of inputfilenames.
%   read_inputfilenames_uc_excel   - Read in of inputfilenames.
%   read_pe_excel                  - Read in of parameters for events.
%   read_uc_excel                  - Read in of use curve files.
%   rsx                            - Rescales x-axis of specified figure, y-axis is unaffected.
%   rsy                            - Rescales y-axis of specified figure, setting of x-axis is unaffected.
%   write_all                      - Write output file.
%   write_results_excel            - Write simuation results to excel file.
