function [strwd] = intwd2strwd(intwd)
%Converts weekday-no. to weekday-string.

clear wdwd;
clear strwd;

wdwd = struct('strwd', {'mo', 'tu', 'we', 'th', 'fr', 'sa', 'su' });

strwd = wdwd(intwd).strwd;

end

