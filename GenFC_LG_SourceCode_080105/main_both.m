%Controls calculation (el and/or hw) and makes simulation results accessible in workspace.

calculate_load(3)

% make some global variable available in workspace
global el_general el_inputfilenames_pe el_inputfilenames_uc el_inputfilenames_occupancy el_Md el_Md_total el_Md_agg

global hw_general hw_inputfilenames_pe hw_inputfilenames_uc hw_inputfilenames_occupancy hw_Md hw_Md_total hw_Md_agg


% save last simulation as LG_LastSimulation.mat in current matlab directory
save LG_LastSimulation.mat