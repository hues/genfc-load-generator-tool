function [] = gen_occupancy_filter_events (intdt)
%Filter events according to occupancy


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
        strMd_total = 'el_Md_total';
    case 2
        strMd = 'hw_Md';
        strMd_total = 'hw_Md_total';
end %switch


%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
clear k
clear rows_delete 
clear events_start
clear a
clear b
clear c
clear kk
clear kkk
clear kkkk
clear n_f

gp = get_general(intdt);

%---filter event list (el_Md.Es resp. hw_Md.Es) with absences; events
%starting within periodes of absence are deleted
for k = 1 : gp.nk   %loop per dwelling
    %prepare for considering operation mode in filtering of events
    clear mode_absence  %operation mode during absences, vector with values 1 or 2, 1: off; 2: on
    clear dc_absence    %column vector containing demand categories which are switched on during absences       
    eval(['mode_absence = ', strMd,'{1,k}.pe.mode_absence;']) %mode_absence = hw_Md{1,k}.pe.mode_absence;
    
    %check consistency of parameters operation mode
    for kkkk = 1 : gp.ndc
        if ~((mode_absence(kkkk) == 1) | (mode_absence(kkkk) == 2))
            msgbox(['Please check parameter ''Operation mode'' in file ...pe.xls for dwelling', num2str(k)])
        end %if
    end %for
    
    %convert mode_absence into dc_absence
    dc_absence = find(mode_absence - 1);
   
    %main filter loop
    eval(['temp = ~isempty(', strMd,'{1,k}.occupancy.As_eff);'])      %temp = ~isempty(hw_Md{1,k}.occupancy.As_eff)   
    if ~isempty(temp)
        eval(['a = size(', strMd, '{1,k}.occupancy.As_eff);'])     %a = size(hw_Md{1,k}.occupancy.As_eff);
        for kk = 1 : a(1)       %loop per row of As_eff

            n_f = kk;

            eval(['events_start = ', strMd, '{1,k}.Es(:,7);'])  %events_start = hw_Md{1,k}.Es(:,7);

            eval(['filter_start = ', strMd, '{1,k}.occupancy.As_eff(n_f,1);'])  %filter_start = hw_Md{1,k}.occupancy.As_eff(n_f,1);
            eval(['filter_end = ', strMd, '{1,k}.occupancy.As_eff(n_f,2);'])    %filter_end = hw_Md{1,k}.occupancy.As_eff(n_f,2);

            rows_delete = find(filter_start < events_start & events_start < filter_end);
            %disp(['potential_deletions ', num2str(size(rows_delete)), '  rowAs', num2str(kk), '  dwelling ', num2str(k)])
   
            %---consider demand categories (filter only some of the categories)
            if ~isempty(dc_absence)  %in case no category is operational during absences then dc_absence is empty

                events_categories_on = [];     %column vector containing the indices of events satisfying the category criteria
                eval(['b = size(', strMd, '{1,k}.Es);'])    %b = size(hw_Md{1,k}.Es);
                for kkk = 1 : b(1)
                    eval(['temp = find(dc_absence == ', strMd, '{1,k}.Es(kkk,5));'])   %temp = ~isempty(find(dc_absence == hw_Md{1,k}.Es(kkk,5)))
                    if ~isempty(temp)
                       events_categories_on = [events_categories_on; kkk];  %append vector events_categories_on filter
                    end %if  
                end %for
                %disp(['e_cat_on ', num2str(size(events_categories_on)), '  rowAs', num2str(kk), '  dwelling ', num2str(k)])

                %filter row_delete, keep only shared elements with events_categories_on 
                if ~isempty(rows_delete)
                    rows_delete_on = [];
                    b = size(rows_delete);
                    for kkk = 1 : b(1)
                        if ~isempty(find(events_categories_on == rows_delete(kkk)))
                           rows_delete_on = [rows_delete_on; kkk];  %append vector rows_delete_on
                        end %if  
                    end %for
                    rows_delete(rows_delete_on,:) = []; 
                    %disp(['prevent_cat_on ', num2str(size(rows_delete_on)), '  rowAs ', num2str(kk), '  dwelling ', num2str(k)])
                end %if

            end %if

            %---message for test purpuses
             %c = size(rows_delete);
             %disp([num2str(c(1)),' events will be deleted'])

            eval([strMd, '{1,k}.Es(rows_delete,:) = [];'])                 %hw_Md{1,k}.Es(rows_delete,:) = [];
        end %for    
    end %if
end %for

%---display confirmation message
disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: Events successfully adapted to occupancy'])
  

end %function