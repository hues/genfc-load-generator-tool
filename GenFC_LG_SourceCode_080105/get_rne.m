function [rne] = get_rne()
%Get equally distributed random number from Pool 'Rne'.

global Rne       %global list with random numbers
global p_rne     %pointer to random number

%---manage pointer to random number -> increment pointer
p_rne = p_rne + 1;

%---access global list with random numbers, random numbers were generated
%ones at start of overall simulation run.
rne = Rne(p_rne);

end %function


