function [Ucc] = gen_Ucc(Uc)
%Generate cumulated use curve based on use curve.

%Initialise
Ucc = [];

%Determine size of time of use curve (no of timesteps, no of demand
%categories)
a = size(Uc);   


%Addition of rows
Ucc(1,:) = Uc(1,:);
for i = 2 : a(1)    
 Ucc(i,:) = Ucc(i - 1,:) + Uc(i,:);
end %end for

end
