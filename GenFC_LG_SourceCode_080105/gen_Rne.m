function [] = gen_Rne(n)
%Fill global pool 'Rne' with equally distributed random numbers.

global Rne       %global list with random numbers
global p_rne     %pointer to random number

%---manage pointer to random number -> initialise pointer
p_rne = 0;

%---generate global list with equally distributed random numbers
Rne = rand(n,1);

end %function