function [rnn] = get_rnn()
%Get normally distributed random number from Pool 'Rnn'.

global Rnn       %global list with random numbers
global p_rnn     %pointer to random number

%---manage pointer to random number -> increment pointer
p_rnn = p_rnn + 1;

%---access global list with random numbers, random numbers were generated
%ones at start of overall simulation run.
rnn = Rnn(p_rnn);

end %function


