function [] = rsy(h, minvalue, maxvalue)
%Rescales y-axis of specified figure, setting of x-axis is unaffected.
%In case of empty call: y-axis is rescaled to fit y-values

%set focus to specified figure
figure(h)

%---store settings of y-axis
v1_axis = axis; %store current min, max values; v1_axis(1): xmin; v1_axis(2): xmax 

%---rescale y-axis
switch nargin
    case 1
        axis tight
        v2_axis = axis;
        minvalue = v2_axis(3);
        maxvalue = v2_axis(4);
        axis([v1_axis(1), v1_axis(2), minvalue, maxvalue])
    case 3
        axis([v1_axis(1), v1_axis(2), minvalue, maxvalue])
    otherwise
        error ('function rsy requires the following arguments: fig-id, [minvalue], [maxvalue]')
end %switch

end %function