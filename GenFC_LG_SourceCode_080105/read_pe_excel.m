function [] = read_pe_excel(intdt)
%Read in of parameters for events.

%---set type dependent variable names
switch intdt
    case 1
        strvariablename = 'el_inputfilenames_pe';
        strMd = 'el_Md';
    case 2
        strvariablename = 'hw_inputfilenames_pe';
        strMd = 'hw_Md';
end %switch

%---initialise
eval(['global ', strvariablename])  %e.g.: global el_inputfilenames_pe
eval(['global ', strMd])  %e.g.: global el_Md
clear dt_Md         %copy of el_Md resp. hw_Md
clear Pe
clear k
clear kk
clear a
clear b
clear filename

gp = get_general(intdt);

%---read in
for k = 1 : gp.nk   %loop per dwelling

    %read in of parameter event file for one dwelling
    eval(['filename = ', strvariablename, '{1,k};'])  %filename = el_inputfilenames_pe{1,k};

    Pe = xlsread(filename,'readpe'); %,'','basic'

    %---read in of parameters for number of events
    dt_Md{1,k}.pe.Enwds = Pe(1:7,1:gp.ndc);

    dt_Md{1,k}.pe.enwd_stdr = Pe(8,1:gp.ndc);
    %replace values '-9999' by 'NaN'
    temp2 = find (dt_Md{1,k}.pe.enwd_stdr == -9999);
    dt_Md{1,k}.pe.enwd_stdr(temp2) = NaN;
    clear temp2       

    dt_Md{1,k}.pe.enwd_season_a = Pe(9,1:gp.ndc);
    %replace values '-9999' by 'NaN'
    temp2 = find (dt_Md{1,k}.pe.enwd_season_a == -9999);
    dt_Md{1,k}.pe.enwd_season_a(temp2) = NaN;
    clear temp2 

    dt_Md{1,k}.pe.enwd_season_ph = Pe(10,1:gp.ndc);
    %replace values '-9999' by 'NaN'
     temp2 = find (dt_Md{1,k}.pe.enwd_season_ph == -9999);
    dt_Md{1,k}.pe.enwd_season_ph(temp2) = NaN;
    clear temp2

    dt_Md{1,k}.pe.enwd_poisson = Pe(11,1:gp.ndc);
    %replace values '-9999' by 'NaN'
     temp2 = find (dt_Md{1,k}.pe.enwd_poisson == -9999);
    dt_Md{1,k}.pe.enwd_poisson(temp2) = NaN;
    clear temp2
    %warning in case of double variation (gaussian and poission)
    for kk = 1 : gp.ndc
        if ~isnan(dt_Md{1,k}.pe.enwd_poisson(kk));
            if ~isnan(dt_Md{1,k}.pe.enwd_stdr(kk));
                warning(['Please be aware that you enabled multiple variation (gaussian and poission) for demand category ', num2str(kk), ' in file .._pe.xls, sheet ''Eventparameter (entry)''']) 
            end %if
        end %if
    end %for
    
    
    %read in of parameters for event power 
    dt_Md{1,k}.pe.ep_mode = Pe(12,1:gp.ndc);
    for kk = 1 : gp.ndc
        switch Pe(12,kk)
            case 1
                dt_Md{1,k}.pe.ep_avg(kk) = Pe(13,kk);
                dt_Md{1,k}.pe.ep_stdr(kk) = Pe(14,kk);
                dt_Md{1,k}.pe.ep_min(kk) = NaN;
                dt_Md{1,k}.pe.ep_max(kk) = NaN;                        
            case 2
                dt_Md{1,k}.pe.ep_min(kk) = Pe(15,kk);
                dt_Md{1,k}.pe.ep_max(kk) = Pe(16,kk);
                dt_Md{1,k}.pe.ep_avg(kk) = NaN;
                dt_Md{1,k}.pe.ep_stdr(kk) = NaN;
            otherwise
                error 'check assignments for parameter ep_mode',
        end %switch
    end %for


    %read in of parameters for event duration 
    dt_Md{1,k}.pe.ed_mode = Pe(17,1:gp.ndc);
    for kk = 1 : gp.ndc
        switch Pe(17,kk)
            case 1
                dt_Md{1,k}.pe.ed_avg(kk) = Pe(18,kk);
                dt_Md{1,k}.pe.ed_stdr(kk) = Pe(19,kk);
                dt_Md{1,k}.pe.ed_min(kk) = NaN;
                dt_Md{1,k}.pe.ed_max(kk) = NaN;
            case 2
                dt_Md{1,k}.pe.ed_min(kk) = Pe(20,kk);
                dt_Md{1,k}.pe.ed_max(kk) = Pe(21,kk);
                dt_Md{1,k}.pe.ed_avg(kk) = NaN;
                dt_Md{1,k}.pe.ed_stdr(kk) = NaN;
            otherwise
                error 'check assignments for parameter ed_mode',
        end %switch
    end %for

    %read in of standby power 
    dt_Md{1,k}.pe.p_standby = Pe(22,1:gp.ndc);

    %read in of operation mode during absences 
    dt_Md{1,k}.pe.mode_absence = Pe(23,1:gp.ndc);
    
    %assing el_Md resp. hw_Md
    eval([strMd,'{1,k}.pe = dt_Md{1,k}.pe;'])     %e.g. el_Md{1,k}.pe = dt_Md{1,k}.pe

    
    %---display confirmation message
    disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: File "', filename,'" successfully read in.'])
    clear filename

end %for     


%---clean up
clear dt_Md
clear Pe
clear k
clear kk
clear a
clear filename
clear p0


end %function