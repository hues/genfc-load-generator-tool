function [] = plot_all (intdt, dwns, graph, stradd)
%Plot curves on level dwelling -> Es(Chs), Ls, Lc.
%Md: workspace variable to plot e.g. el_Md
%dwns: vector containing dwelling numbers, e.g. [1, 3, 4] or [1:5];
%dwns: special value 0 (instead of vector) to plot total of all dwellings
%graph: curve(s) to plot; values: 'Es', 'Ls', 'Lc', optional, if not
%present all curves are plotted
%stradd: values: 'add', adds graphs to current figure; optional, if not
%present ploting in new figure window
%legends: always overwritten, also in 'add' mode, use figure tool to see
%previous legends


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
        strMd_total = 'el_Md_total';
    case 2
        strMd = 'hw_Md';
        strMd_total = 'hw_Md_total';
end %switch


%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
eval(['global ', strMd_total])  %e.g.: global el_Md_total
clear dt_Md         %copy of el_Md resp. hw_Md
clear dt_Md_total         %copy of el_Md_total resp. hw_Md_total
clear k
clear nk
clear b
clear legendtext    %onedimensional cell array with string entries
clear figure_h
clear axes_h
clear title_h

gp = get_general(intdt);

axes_h = [];
legendtext ={};


%---assign dt_Md, if el_Md resp. hw_Md is available (simulation already executed)
try
    switch intdt
        case 1
            el_Md{1,1}.Es;
            dt_Md = el_Md;
            dt_Md_total = el_Md_total;
        case 2
            hw_Md{1,1}.Es;
            dt_Md = hw_Md;
            dt_Md_total = hw_Md_total;
    end %switch
catch
    msgbox('Please run simulation first.')
    return
end %try


%---control of figure window and plot framework
switch nargin    
    case {0, 1}
        %---case of missing arguments
        error 'Argument(s) missing, please specify 'dt_Md', dwns, graph, opt:'add''
        return
    case 2
        if strcmp(dwns, 'add') ||  strcmp(dwns, 'Es') ||  strcmp(dwns, 'Ls') ||  strcmp(dwns, 'Lc') ||  strcmp(dwns, 'all')   
            error 'Argument(s) missing, please specify 'dt_Md', dwns, graph, opt:'add''
            return
        end %if
        
        graph = 'all';
        figure_h = figure;
    case 3
        if strcmp(graph, 'add')  %test third argument
            graph = 'all';
        else
            figure_h = figure;
        end %if
    case 4
        if strcmp(stradd, 'add')  %test forth argument
            %nothing
        else
            figure_h = figure;
        end %if
    otherwise
        error 'To many argument(s), please specify 'dt_Md', dwns, graph, opt:'add''
        return
end %switch

%---get current window respectively the window just generated
figure_h = gcf;

%---store hold state for later restoring
hold_state = ishold;

%---set temporarly hold state to on
hold on

%---set properties of window
set(figure_h, 'Name', 'GenFC load generator: simulation plot window'...
               )   %if figure number not desired -> add: 	'NumberTitle', 'on'      
           
%---plotting graphs    
        
%initialise dwelling loop
a = size (dwns);
nk = a(2);     
%check consistency
if gp.nk < nk
    msgbox(['Dwelling number must not exceed ', num2str(gp.nk)])
end %if

if dwns(1)~=0     %plots for individual dwellings
        
    %dwelling loop
    for k = 1 : nk  

        %plot graph Chs
        if strcmp(graph, 'Es') || strcmp(graph, 'all')
            x = (dt_Md{1,dwns(k)}.Chs(:,7)) ./ 24;
            y = dt_Md{1,dwns(k)}.Chs(:,9);
            axes_h(end + 1)= stem (gca, x, y, ':'); %dotted line
            legendtext{end + 1} = ['Es', ', dw ', num2str(dwns(k))];
        end %if                     

        %plot graph Ls
        if strcmp(graph, 'Ls') || strcmp(graph, 'all')
            x = (dt_Md{1,dwns(k)}.Ls(:,7)) / 24;
            y = dt_Md{1,dwns(k)}.Ls(:,9);
            axes_h(end + 1) = stairs (x, y, '--');   %dashed line
            legendtext{end + 1} = ['Ls', ', dw ', num2str(dwns(k))];
        end %if                                    

        %plot graph Lc
        if strcmp(graph, 'Lc') || strcmp(graph, 'all')
            x = (dt_Md{1,dwns(k)}.Lc(:,7)) / 24;
            y = dt_Md{1,dwns(k)}.Lc(:,9);
            axes_h(end + 1) = stairs (x, y, '-');   %solid line
            legendtext{end + 1} = ['Lc', ', dw ', num2str(dwns(k))];
        end %if
          
    end %for

else    %plot for total over all dwellings
    
    %plot graph Chs
    if strcmp(graph, 'Es') || strcmp(graph, 'all')
        x = (dt_Md_total.Chs(:,7)) ./ 24;
        y = dt_Md_total.Chs(:,9);
        axes_h(end + 1)= stem (gca, x, y, ':'); %dotted line
        legendtext{end + 1} = ['Es', ', mfh '];
    end %if                     

    %plot graph Ls
    if strcmp(graph, 'Ls') || strcmp(graph, 'all')
        x = (dt_Md_total.Ls(:,7)) / 24;
        y = dt_Md_total.Ls(:,9);
        axes_h(end + 1) = stairs (x, y, '--');   %dashed line
        legendtext{end + 1} = ['Ls', ', mfh '];
    end %if                                    

    %plot graph Lc
    if strcmp(graph, 'Lc') || strcmp(graph, 'all')
        x = (dt_Md_total.Lc(:,7)) / 24;
        y = dt_Md_total.Lc(:,9);
        axes_h(end + 1) = stairs (x, y, '-');   %solid line
        legendtext{end + 1} = ['Lc', ', mfh '];
    end %if    
    
end %if


%plotting legends
legend off
legend(axes_h, legendtext, -1);

        
%---coloring of graphs 

%color space setting
e = size(axes_h);
colormap(jet(round(e(2) .* 4)));    %colors space with (ndc * 1.5) colors, upper part of color space (red colors) not used)  
cmap = colormap;

%set graph color
for k = 1 : e(2)
    color_rgb = cmap(k + round(e(2) .* 3), :);   %pick color in upper forths of color space (red colors)
    set(axes_h(k), 'Color', color_rgb);
end %for 


%---labeling of figure (exept: legend, see above)
    
if  dt_Md{1,1}.Es(1,4) == 1 %electric demand (demand type is deducted from first data row of dwelling 1 of Es)
    title('Electric load', 'FontSize', 14)
    xlabel('Time [d]')
    ylabel('Power [kW]')
end %if

if  dt_Md{1,1}.Es(1,4) == 2 %hot water demand (demand type is deducted from first data row of dwelling 1 of Es)
    title('Hot water load', 'FontSize', 14)
    xlabel('Time [d]')
    ylabel('Flow [l/h]')
end %
axis('tight') 
           
           
%---reset hold state to previous value (value before function execution)        
if hold_state
    hold on
else
    hold off
end %if


end %function


    %---some not used commands that might usefull
    % scrsz = get(0,'ScreenSize');    %ScreenSize: ?,?,no of pixels x, no of pixels y
    % figure('Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2]) %[left, bottom, width, height]
    % get(hf)    %display figure properties
    %[legend_h, object_h, plot_h, legendtext] = legend(gca);  %read in of current legend texts into variable legendtext
    % plotedit('hidetoolsmenu')   ->funktioniert nicht!
    % grid on
    % set(get(gcf,'CurrentAxes'), 'Title', title_h)