function [] = read_inputfilenames_uc_excel(intdt)
%Read in of inputfilenames.


%---set type dependent variable names
switch intdt
    case 1
        strfilename = 'el_inputfilenames.xls';
        strfilename2 = 'el_general.xls';  
        strvariablename = 'el_inputfilenames_uc';
    case 2
        strfilename = 'hw_inputfilenames.xls';  
        strfilename2 = 'hw_general.xls';  
        strvariablename = 'hw_inputfilenames_uc';
end %switch


%---initialise
eval(['global ', strvariablename])  %e.g.: global el_inputfilenames_uc
clear filename
clear gp
clear k
clear a
clear b


%---read in of inputfilenames for event parameters

%load filenames
eval(['filename = ''',  strfilename, ''';'])    %filename = 'el_inputfilenames.xls';        
eval(['[num, ',  strvariablename, '] = xlsread(filename,''readfnuc'');'])          %[num, el_inputfilenames_pe] = xlsread(filename,'readfnpe');
eval([strvariablename, ' = ', strvariablename, ''';'])    %transpose variable
clear num
                
%check for consistency with global parameter nk
gp = get_general(intdt);

eval(['[a, b] = size(', strvariablename, ');'])     %[a, b] = size(el_inputfilenames_pe);
if isequal(gp.nk, b)
    %do nothing, check i.o.
else
    %warning message
    eval(['warning(''Table of filenames (file ', strfilename, ', sheet use curves) not consistent with number of dwellings in file ', strfilename2, '.'')'])
end %if

%display confirmation message
disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: Sheet "readuc" of file "', filename,'" successfully read in.'])


%---clean up
clear filename
clear gp
clear k
clear a
clear b


end %function

  