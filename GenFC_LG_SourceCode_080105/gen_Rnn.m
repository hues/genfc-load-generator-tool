function [] = gen_Rnn(n)
%Fill global pool 'Rnn' with normally distributed random numbers.

global Rnn       %global list with random numbers
global p_rnn     %pointer to random number

%---manage pointer to random number -> initialise pointer
p_rnn = 0;

%---generate global list with normally distributed random numbers
Rnn = randn(n,1);

end %function