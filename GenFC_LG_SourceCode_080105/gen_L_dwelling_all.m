function [S] = gen_L_dwelling_all (sd, ed, intdt, intwd0, ts_uc, ts_lc, Uccs, paraevents, Es) 
%Generate load for one dwelling, all demand categories.

clear p0;

p0 = sum(paraevents.p_standby); %sum of standby power of all demand categories
[Ls, Chs] = gen_loadserie_all (Es, p0);


%--------------------------
%Generation of total load curve (eletric resp. dhw)
%result: load serie Ls (lno, dn, intwd, intdt ,dc, t, p)
%lno: row number of Lc
%dn: day number
%intwd: week day (mo: 1, tu: 2, ...)
%intdt: demand type (electric: 1, dhw: 2)
%dc: demand category
%p: power

Lc = gen_loadcurve_all (Ls, sd, ed, intdt, ts_lc, p0);  



%----------------------
S(1).Es =  Es;   %S contains only S(1), overwritten for each dwelling run
S(1).Chs =  Chs;
S(1).Ls =  Ls;
S(1).Lc =  Lc;

end    % function gen_load_dwelling_all 
