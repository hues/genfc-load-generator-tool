function [ Es_dcs ] = get_events_dwelling_dcs (intdt, Es, dcs )
%Retrieve event series for selected demand categories.
%dcs  demand categories(s), vector, e.g. [3, 5, 6, 7]; optional

clear k
clear nk
clear m
clear n
clear Es_dc
clear Es_dcs
clear gp

switch nargin
    case 2
        gp = get_general(intdt);
        
        nk = gp.ndc;                
        
        for k = 1 : nk
            Es_dc = get_events_dwelling_dc (k);
            Es_dcs(k) = {Es_dc};
        end %for        
    case 3
        m = size (dcs);
        nk = m(2);

        for k = 1 : nk
            Es_dc = get_events_dwelling_dc (dcs(k));
            Es_dcs(k) = {Es_dc};
        end %for
        
    otherwise
end %switch
    

    
    
function [Es_dc] = get_events_dwelling_dc (dc)

clear Es_dc
clear a

a = find ((Es(:,5)) == dc);
Es_dc = Es(a,:); %rows that fullfill criteria dc   
    
end %function get_events_dwelling_dc


end %function get_events_dwelling_dcs
