function varargout = loadgengui_basic(varargin)
% loadgengui_basic M-file for loadgengui_basic.fig
%      loadgengui_basic, by itself, creates a new loadgengui_basic or raises the existing
%      singleton*.
%
%      H = loadgengui_basic returns the handle to a new loadgengui_basic or the handle to
%      the existing singleton*.
%
%      loadgengui_basic('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in loadgengui_basic.M with the given input arguments.
%
%      loadgengui_basic('Property','Value',...) creates a new loadgengui_basic or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before loadgengui_basic_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to loadgengui_basic_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help loadgengui_basic

% Last Modified by GUIDE v2.5 22-Dec-2007 01:07:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @loadgengui_basic_OpeningFcn, ...
                   'gui_OutputFcn',  @loadgengui_basic_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before loadgengui_basic is made visible.
function loadgengui_basic_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to loadgengui_basic (see VARARGIN)

% Choose default command line output for loadgengui_basic
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Custom initialisation function
initialize_gui(hObject, handles, false);


% UIWAIT makes loadgengui_basic wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = loadgengui_basic_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function initialize_gui(fig_handle, handles, isreset)
set(handles.uipanel_loadselection, 'SelectedObject', handles.rb_both);
handles.loadtype = 3;
handles.loadtype2 = 1;
handles.plottype = 1;
handles.plotgraph = 1;
handles.rescale = 'x';

handles.fieldrescale = '';
handles.fieldplotarguments = '';

% Update handles structure
guidata(handles.figure1, handles);


% --------------------------------------------------------------------
function uipanel_loadselection_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_LoadSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'Tag')   % Get Tag of selected object
    %hObject refers to the selected radio button 
    case 'rb_electric'
        handles.loadtype = 1;
    case 'rb_hotwater'
        handles.loadtype = 2;
    case 'rb_both'
        handles.loadtype = 3;
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pb_startsimulation.
function pb_startsimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pb_startsimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch handles.loadtype
    case 1
        main_el();
    case 2
        main_hw();        
    case 3
        main_both();
end %switch

msgbox('Simulation completed, please check messages in DOS window', 'GenFC load generator.');



% --- Executes on button press in rb_electric.
function rb_electric_Callback(hObject, eventdata, handles)
% hObject    handle to rb_electric (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rb_electric



function edit_plotarguments_Callback(hObject, eventdata, handles)
% hObject    handle to edit_plotarguments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_plotarguments as text
%        str2double(get(hObject,'String')) returns contents of edit_plotarguments as a double

handles.fieldplotarguments = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function edit_plotarguments_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_plotarguments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_savelastsimulation.
function pb_savelastsimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pb_savelastsimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on button press in pb_loadlastsimulation.
function pb_loadlastsimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pb_loadlastsimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load LG_LastSimulation.mat;
load_lastsimulation;
msgbox('Last simulation loaded, ready for plotting.');



% --------------------------------------------------------------------
function uipanel_loadselection2_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_loadselection2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'Tag')   % Get Tag of selected object
    %hObject refers to the selected radio button 
    case 'rb_electric2'
        handles.loadtype2 = 1;
    case 'rb_hotwater2'
        handles.loadtype2 = 2;
end

% Update handles structure
guidata(hObject, handles);


% --------------------------------------------------------------------
function uipanel_plotselection_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_plotselection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'Tag')   % Get Tag of selected object
    %hObject refers to the selected radio button 
    case 'rb_all'
        handles.plotfunction = 1;
    case 'rb_pc'
        handles.plotfunction = 2;
end

% Update handles structure
guidata(hObject, handles);



function edit_rescale_Callback(hObject, eventdata, handles)
% hObject    handle to edit_rescale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_rescale as text
%        str2double(get(hObject,'String')) returns contents of edit_rescale as a double
handles.fieldrescale = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_rescale_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_rescale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function uipanel_graphselection_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_graphselection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'Tag')   % Get Tag of selected object
    %hObject refers to the selected radio button 
    case 'rb_es'
        handles.plotgraph = 1;
    case 'rb_ls'
        handles.plotgraph = 2;
    case 'rb_lc'
        handles.plotgraph = 3;
    case 'rb_all'
        handles.plotgraph = 4;   
end

% Update handles structure
guidata(hObject, handles);




% --- Executes on button press in pb_plot.
function pb_plot_Callback(hObject, eventdata, handles)
% hObject    handle to pb_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pb_plot
strplotfield = handles.fieldplotarguments;

switch handles.plottype
    case 1
        strplottype ='all(';
    case 2
        strplottype ='dcs(';
end %switch

switch handles.loadtype2
    case 1
        strloadtype2 = '1,';
    case 2
        strloadtype2 = '2,';
end %switch

switch handles.plotgraph
    case 1
        strplotgraph = ',''Es'')';
    case 2
        strplotgraph = ',''Ls'')';
    case 3
        strplotgraph = ',''Lc'')';
    case 4
        strplotgraph = ')';
end %switch

strtranfer = ['plot_', strplottype, strloadtype2, strplotfield, strplotgraph];

%msgbox(strtranfer)     %for test purposes
eval(strtranfer);


% --- Executes on button press in pb_rescale.
function pb_rescale_Callback(hObject, eventdata, handles)
% hObject    handle to pb_rescale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pb_rescale
strrescale = ['rs', handles.rescale, '(', handles.fieldrescale, ')']

eval(strrescale);

% --------------------------------------------------------------------
function uipanel_rescaling_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_rescaling (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'Tag')   % Get Tag of selected object
    %hObject refers to the selected radio button 
    case 'rb_rsx'
        handles.rescale = 'x';
    case 'rb_rsy'
        handles.rescale = 'y';
end

% Update handles structure
guidata(hObject, handles);





% --- Executes on button press in rb_catagg.
function rb_catagg_Callback(hObject, eventdata, handles)
% hObject    handle to rb_catagg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rb_catagg


% --- Executes on button press in rb_catpar.
function rb_catpar_Callback(hObject, eventdata, handles)
% hObject    handle to rb_catpar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rb_catpar


% --------------------------------------------------------------------
function uipanel_plottype_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_plottype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'Tag')   % Get Tag of selected object
    %hObject refers to the selected radio button 
    case 'rb_catagg'
        handles.plottype = 1;
    case 'rb_catpar'
        handles.plottype = 2;
end

% Update handles structure
guidata(hObject, handles);

