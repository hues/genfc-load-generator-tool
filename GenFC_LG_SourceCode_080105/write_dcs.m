function [] = write_dcs ( name_of_variable, values_of_variable, dwn, dc, intdt) 
%Write output file.
%dc: demand category; value 0 in case of total for all categories


%---initialise
clear L
clear m
clear n
clear a
clear b
clear arrRowsToPrint


%---set filename
strL = strcat (intdt2strdt(intdt), '_dw', num2str(dwn), '_dc', num2str(dc), '_', name_of_variable, '.txt');


%set rows to print according to variable name
switch name_of_variable
    case 'Es'
        arrRowsToPrint = [7,9,8,11,5];  %t1period, p, d, t2period, dc
    case 'Ls'
        arrRowsToPrint = [7,9]; %t1period, p
    case 'Lc'        
        arrRowsToPrint = [7,9]; %t1period, p
end %switch
[a,b] = size(arrRowsToPrint); 


%---format and print rows
%prepare file
[m,n] = size(values_of_variable);
fid = fopen(strL,'w');

%write columns to file
for j = 1:m    %repeat for each row
    if ~isnan(values_of_variable(j,7));        %printout only real event data, skip rows with NaN
        for i = 1:b    %repeat for each column
            if arrRowsToPrint(i) <= 5
                fprintf(fid,'%6.0f\t',values_of_variable(j,arrRowsToPrint(i)));
            elseif arrRowsToPrint(i) <= 7
                fprintf(fid,'%6.2f\t',values_of_variable(j,arrRowsToPrint(i)));
            else
                fprintf(fid,'%6.2f\t',values_of_variable(j,arrRowsToPrint(i)));
            end %end if
        end %end for
        fprintf(fid,'\n');
    end %if
end %end for

%close file
fclose(fid);


end %function