function [] = write_results_excel(intdt)
%Write simuation results to excel file.
%File is generated, if it doesn't exist already.


%---set type dependent variable names
switch intdt
    case 1
        strMd = 'el_Md';
        outputfilename = 'el_results_simulation.xls';
    case 2
        strMd = 'hw_Md';
        outputfilename = 'hw_results_simulation.xls';
end %switch


%---initialise
eval(['global ', strMd])  %e.g.: global el_Md
clear dt_Md         %copy of el_Md resp. hw_Md
clear M_demand  %events: total demand
clear M_ep      %events: mean power
clear M_ed      %events: mean duration
clear m_demand_total
clear m_ep_total
clear m_ed_total
clear k kk
clear ndwn
clear ndc
clear gp
clear a
clear no_r


%---assign dt_Md, if el_Md resp. hw_Md is available (simulation already executed)
try
    switch intdt
        case 1
            el_Md{1,1}.Es;
            dt_Md = el_Md;
        case 2
            hw_Md{1,1}.Es;
            dt_Md = hw_Md;
    end %switch
catch
    msgbox('Please run simulation first.')
    return
end %try


%---calculate results per categories (based on Lc_dcs)
gp = get_general(intdt);
nk = gp.nk;        
ndc = gp.ndc;   

for k = 1 : gp.nk  %loop dwelling 
    
    for kk = 1 : gp.ndc  %loop category
        
        %---calculate demand
        M_demand{k, 1} = ['dwelling ', num2str(k)];    %column 1 with string 'dwelling no'

        M_demand{k, kk + 1} = integrate_loadcurve(dt_Md{1,k}.Lc_dcs{1,kk});   %column for each category (demand figure)

        %project demand to 1 year
        M_demand{k, kk + 1} = M_demand{k, kk + 1} .* 364 ./ (gp.ed - gp.sd + 1);
        
        %determine number of rows with event duration not zero (no_r), used later for calculation of means        
        a = size(find(dt_Md{1,k}.Es_dcs{1,kk}(:,8)~=0)); no_r = a(1);  

        %---calculate mean event duration        
        M_ed{k, 1} = ['dwelling ', num2str(k)];    %column 1 with string 'dwelling no'
        M_ed{k, kk + 1} = sum(dt_Md{1,k}.Es_dcs{1,kk}(:,8)) ./ no_r;   %column for each category (demand figure)
        %replaces in Matrix dt_Md_ed all values "'NaN'" by "-9999"
        if (find(isnan(M_ed{k, kk + 1}))) == 1
            M_ed{k, kk + 1} = -9999;
        end %if         

        %---calculate mean event power
        M_ep{k, 1} = ['dwelling ', num2str(k)];    %column 1 with string 'dwelling no'
        M_ep{k, kk + 1} = sum(dt_Md{1,k}.Es_dcs{1,kk}(:,9)) ./ no_r;   %column for each category (demand figure)
        %replaces in Matrix M_ep all values "'NaN'" by "-9999"
        if (find(isnan(M_ep{k, kk + 1}))) == 1
            M_ep{k, kk + 1} = -9999;
        end %if 

    end %for
   
end %for


%---calculate results for total (based on Lc)
for k = 1 : nk  %loop dwelling
     
    for kk = 1 : ndc  %loop category
        
        %---calculate demand
        m_demand_total{k, 1} = ['dwelling ', num2str(k)];    %column 1 with string 'dwelling no'
        m_demand_total{k, 2} = integrate_loadcurve(dt_Md{1,k}.Lc);   %column for each category (demand figure)
        %project demand to 1 year
        m_demand_total{k, 2} = m_demand_total{k, 2} .* 364 ./ (gp.ed - gp.sd + 1);

        %determine number of rows with event duration not zero (no_r), used later for calculation of means        
        a = size(find(dt_Md{1,k}.Es(:,8)~=0)); no_r = a(1);  
        
        %---calculate mean event duration        
        m_ed_total{k, 1} = ['dwelling ', num2str(k)];    %column 1 with string 'dwelling no'
        m_ed_total{k, 2} = sum(dt_Md{1,k}.Es(:,8)) ./ no_r;   %column for each category (demand figure)
        %replaces in Matrix md_ed_total all values "'NaN'" by "-9999"
        if (find(isnan(m_ed_total{k, 1}))) == 1
            m_ed_total{k, 2} = -9999;
        end %if         

        %---calculate mean event power
        m_ep_total{k, 1} = ['dwelling ', num2str(k)];    %column 1 with string 'dwelling no'
        m_ep_total{k, 2} = ['dwelling ', num2str(k)];    %column 1 with string 'dwelling no'
        m_ep_total{k, 2} = sum(dt_Md{1,k}.Es(:,9)) ./ no_r;   %column for each category (demand figure)
        %replaces in Matrix md_ep_total all values "'NaN'" by "-9999"
        if (find(isnan(m_ep_total{k, 1}))) == 1
            m_ep_total{k, 2} = -9999;
        end %if 
         
    end %for
    
    %---fill up matrixes with zeros up to maximal size in excel files
    %(in order to overwrite results from previous calculations)
    
    %overwrite rows: adds rows with zeros up to row number 125
for k = 1 : 125
    for kk = (ndc + 1) : 25
        M_demand{k, kk + 1} = [];
        M_ed{k, kk + 1} = [];
        M_ep{k, kk + 1} = [];

        m_demand_total{k, kk + 1} = []; 
        m_ed_total{k, kk + 1} = []; 
        m_ep_total{k, kk + 1} = []; 
    end %for
end %for
    
    %overwrite columns: adds coloms with zeros up to column number 25
    for k = (nk + 1) : 125
        for kk = 1 : 25
             M_demand{k, kk + 1} = [];
             M_ed{k, kk + 1} = [];
             M_ep{k, kk + 1} = [];
        end %for
    end %for
    
end %for


%---write operation (categories and total)
try
    %---write operation for categories
    xlswrite(outputfilename, M_demand, 'demand', 'A5');
    xlswrite(outputfilename, M_ed, 'ed', 'A5');
    xlswrite(outputfilename, M_ep, 'ep', 'A5');

    %---write operation for total
    xlswrite(outputfilename, m_demand_total, 'demand_total', 'A5');
    xlswrite(outputfilename, m_ed_total, 'ed_total', 'A5');
    xlswrite(outputfilename, m_ep_total, 'ep_total', 'A5');
    
    %---write operation: confirmation message
    disp([datestr(now, 'yyyy-mm-dd HH:MM:SS'), ' Confirmation: Totals per dwelling were written sucessfully to file ', outputfilename, '.'])
catch
    error('Writing to xls-file was not successful.')
end %try   



%---clean up
clear dt_Md


end %function