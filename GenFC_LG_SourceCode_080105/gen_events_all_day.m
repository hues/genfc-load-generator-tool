function [Es_day] = gen_events_all_day (dn, intwd, intdt, ndc, ts_uc, Uccs, paraevents)
%Generation of events for all demand categories of electric demand resp. dhw demand, yet with fixed.
%Duration, within one day
%All subfunctions of gen_events_all_day share input arguments

%Initialisation
Es_day= [];
ucc = []; %daily cumulated use curve for any demand category or weekday
intdc = [];
clear temp;


Ucc = Uccs{1,intwd};    %assignement of variable Ucc corresponding to weekday

enwd = paraevents.Enwds(intwd,:);   %expectation value of number of events for each category on weekday intwd


%---prepare variables for statistical and seasonal variation of number of
%events
clear enwd_stdr
clear enwd_season_a
clear enwd_season_ph
enwd_stdr = paraevents.enwd_stdr;
enwd_season_a = paraevents.enwd_season_a;
enwd_season_ph = paraevents.enwd_season_ph;
enwd_poisson = paraevents.enwd_poisson;


%---Populate Es_day
for k = 1 : ndc;
    
    intdc = k;
    ucc = Ucc(:,k);
    en = enwd(k);

    %---randomise number of events (en) with gaussian distribubiton defined by enwd_stdr
    if isnan(enwd_stdr(k))
        %do nothing
    else
        %core of randomise number of events (en) with gaussian distribubiton defined by enwd_stdr 
        clear rn
        rn = get_rnn();  %normally distributed random number with mean = 0 and STD = 1 
        en = en + ((rn .* enwd_stdr(k)) .* en);
        clear rn
    end %if
    
    
    %---seasonal variation of number of events
    if isnan(enwd_season_a(k)) && isnan(enwd_season_ph(k))
        %no seasonal variation, both variation parameters not set
        %do nothing, en kept unchanged
    else
        if isnan(enwd_season_a(k)) || isnan(enwd_season_ph(k))  %check if there is an unvalid argument
            error 'One argument for seaonal variation of number of events is missing.'
        else
            % core of seasonal variation
            ph = 0.25 - ((enwd_season_ph(k) + 1) ./ 364);
            en = en .* (1 + enwd_season_a(k) .* sin(((dn ./ 365) + ph) .* 2 .* pi));
            %for tests: x = 0 : 0.01 : 1 ; plot(x,1 + a .* sin((x + ph) .* 2 .* pi))            
        end %if
    end %if

    
    %---prevent negative values for number of events
    if en < 0
        en = 0;
        continue    %next loop
    end %if
    
    
    %---variation based on poisson distribution
    %integer part of daily no. of events (en) is varied according to poisson distribution, non
    %integer part is than re-added added
        if isnan(enwd_poisson(k))
        %do nothing
        else
            if enwd_poisson(k) == 1
                en_int = round (en - rem(en,1));
                en_fra = rem(en,1);
                if en_int >= 1   %poisson inverse cumlative distribution function only defined for values >= 1
                    en = icdf('Poisson',rand,en_int);    %variation according to poisson distribution; icdf: inverse cumlative distribution function;
                    en = en + en_fra;
                end %if
            else
                error 'Please check value for enabling poission distribution in file .._pe.xls'
            end %if
        end %if
        
       
    %---append event list 
    temp = gen_events_day ();
    Es_day= [Es_day;temp];     %each loop run adds all the events of one demand category to the list Es_day(events within one day, all categories)  
    %E{1:end,1:end} %Anzeige alle Werte

    
    %----clean up loop
    ucc = []; %daily cumulated use curve for any demand category or weekday
    intdc = [];
    clear temp;

end %for

clear enwd_stdr



function [Es_demand] = gen_events_day ()
%Generation of events for a specified demand category, yet with fixed duration, within one day
%
%inputs:
%intdt: demand type (1: electric; 2: domestic hot water)
%intdc: demand category (1, 2,... , en), to discuss: also w1,...,wn 
%intwd: week day
%ts_uc: timestep use curve
%ucc: daily cumulated use curve for any demand category or weekday; ucc = Ucc(i,:)
%en: expectation value of number of events for category
%
%returns some events: Es_demand (structure), attributes: start time (t1), duration (d), power (p)  
%Scaling to expectation value of number of events per day.
%The number of events returned are either ganzzahliger Anteil des expectation
%value oder ganzzahliger Anteil + 1
%Principle: Repeat single events loop as many times as ganzzahliger Wert
%des Erwartungswertes, zusätzlicher Loop findet mit Wahrscheinlichkeit des
%Nachkommawertes des Erwartunswertes statt.


%Initialisation
Es_demand = [];
clear kk

en_int = round (en - rem(en,1));
en_fra = rem(en,1);

for kk = 1 : en_int
  Es_demand(kk,:) =  gen_event();
end  %end for

%conditional generation of one event with probability rem(en,1)
rn = get_rne();
if (rn < en_fra)
  Es_demand(end + 1,:) =  gen_event();
end  %end if 

%---generation of one event
function [e] = gen_event()
%Subfunction of gen_events_day 
%Single event generation for a specified demand category, yet with fixed duration, within one day
%input: demand category (e1, e2,... , en) weekday, timestep
%external resources: e.g. Ucc_mo,  
%output: et
%e_t1, e_t2: any single time of event (within one day, start resp. stop time)


%Initialisation
e = [];
clear e_t1
clear e_d

%Body
e(1) =  NaN;
e(2) = dn;
e(3) = intwd;
e(4) = intdt;           %demand type
e(5) = intdc;           %demand category

e_t1day = gen_event_t1();  %event, start time
e(6) = e_t1day;             

e(7) = NaN;   %e_t1period

e_d = gen_event_d();     %event, duration
e(8) = e_d;             

e(9) = gen_event_p();   %event, power

e(10) = e_t1day + e_d;      %event, stop time, (t2day), attention: can excede 24 h, values > 24 are admitted.

e(11) = NaN;             %event, stop time, (t2periode)

end %end gen_event

function [e_t1day] = gen_event_t1()
%Subfunction of gen_event
%return event start time

%Initialise
clear rn1    %random number 1, used for determination of time interval
clear rn2    %random number 2, used for determination of time within time interval

%Random number generation
rn1 = get_rne();
rn2 = get_rne();

%Determine time of event, example for category e3
%Term (rn2 * ts_uc): to account for homogeneous distribution over the picked time intervall (and not starting
%time of use curve time interval )
e_t1day = (find(ucc > rn1, 1) * ts_uc) + (rn2 * ts_uc);
end %end gen_event_t1

function [e_d] = gen_event_d()
%Subfunction of gen_event
%return event duration

    switch paraevents.ed_mode(1,intdc)
        case 1             
            clear rn1
            clear ed_avg
            clear ed_stdr
            
            rn1 = get_rnn();  %Normally distributed random number with mean = 0 and STD = 1 
            ed_avg = paraevents.ed_avg(1,intdc);
            ed_stdr = paraevents.ed_stdr(1,intdc);

            e_d = ed_avg + (rn1 .* ed_stdr .* ed_avg);
            
            %prevent event durations smaller than 1 second (1/3600 h) as load
            %generation algorithm assumes event durations > 0
            if e_d < (1/3600)
                e_d = 1/36000;   
            end %if
            
            clear rn1
            clear ed_avg
            clear ed_stdr
        case 2
            clear rn1
            clear ed_min
            clear ed_max

            rn1 = get_rne();  %Equally distributed random number within intervall 0 to 1
            ed_min = paraevents.ed_min(1,intdc);
            
            %check ed_min
            if ed_min < (1/36000)
                error ('minimum event duration allowed is 1/3600 h, please check ed_min')
            end %if
                       
            ed_max = paraevents.ed_max(1,intdc);

            e_d = ed_min + (rn1 .* (ed_max - ed_min));
            
            clear rn1
            clear ed_min
            clear ed_max
    end %switch 
    
end %end gen_event_d


function [e_p] = gen_event_p()
%Subfunction of gen_event
%return event power

    switch paraevents.ep_mode(1,intdc)
        case 1             
            clear rn1
            clear ep_avg
            clear ep_stdr
            
            rn1 = get_rnn();  %Normally distributed random number with mean = 0 and STD = 1 
            ep_avg = paraevents.ep_avg(1,intdc);
            ep_stdr = paraevents.ep_stdr(1,intdc);

            e_p = ep_avg + (rn1 .* ep_stdr .* ep_avg);
            
            %prevent negative values for event power
            if e_p < 0
                e_p = 0;   
            end %if
            
            clear rn1
            clear ep_avg
            clear ep_stdr
        case 2
            clear rn1
            clear ep_min
            clear ep_max

            rn1 = get_rne();  %Equally distributed random number within intervall 0 to 1
            ep_min = paraevents.ep_min(1,intdc);
            
            %check ep_min
            if ep_min < 0
                error ('event power must >= 0, please check ed_min')
            end %if
                        
            ep_max = paraevents.ep_max(1,intdc);

            e_p = ep_min + (rn1 .* (ep_max - ep_min));
            
            clear rn1
            clear ep_min
            clear ep_max
    end %switch
    
end %end gen_event_p

end %end gen_events_day 

end %end gen_events_all_day 